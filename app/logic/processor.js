var campaigns = require('../../api/campaigns.js');
var accounts = require('../../api/accounts.js');
var mongojs = require('mongojs');
var ObjectId = mongojs.ObjectId;
var dbConn = require('../../api/db').connection;

function campaignProcessor(allCampaigns, allCreatives, callback) {

	//Campaign List Loop
	var updateLoop = [];
	//console.log(allCampaigns);
	for (var i=0; i < allCampaigns.length; i++) {
		if (allCampaigns[i].type == 'platform' || allCampaigns[i].type == 'external') {
			console.log('platform record, skip');
		} else {
			var campaignRecord = allCampaigns[i];	
			var impressionTotal = 0;
			var clickTotal = 0;	
			var campStatus = '';

			campaignRecord.type = 'full';
			if (campaignRecord.impressionHistory) {
				if(campaignRecord.impressionHistory.length > 0) {
					for (var j=0; j<campaignRecord.impressionHistory.length; j++) {
							impressionTotal += campaignRecord.impressionHistory[j].impressions;
							clickTotal += campaignRecord.impressionHistory[j].clicks;
					}
					campaignRecord.totalImpressions = impressionTotal;
					campaignRecord.totalClicks = clickTotal;
				}				
			}

			if (campaignRecord.sitescout_status == 'online') {
				campStatus = 'Active';
			} else if (campaignRecord.sitescout_status == 'offline') {
				if (campaignRecord.sitescout_review == 'pending' || campaignRecord.sitescout_review == 'hold') {
					campStatus = 'Pending';
				} else {
					campStatus = 'Inactive';
				}
			} else if (campaignRecord.sitescout_status == 'archived') {
				campStatus = 'Archived';
			} else {
				campStatus = 'Pending';
			}

			//Check Sitescout Tag
			var cmapPixelId = '';
			var regextest = new RegExp('[a-z]+[:.].*?\\"');
			if (campaignRecord.sitescout_tag != null) {
				var n = campaignRecord.sitescout_tag.match(regextest);
				console.log('sitescout_tag: ' + campaignRecord.sitescout_tag);

				if (n.length >= 0) {
					var m = n[0].replace(/"/i,"");
					var o = m.split("/");
					var p = o.length -1;

					cmapPixelId = o[p];
				} else {
					cmapPixelId = "N/A";
				}			
			} else {
				cmapPixelId = "N/A";
			}

			//Check Conversion Tag
			var convertPixelId = '';
			var regextest = new RegExp('[a-z]+[:.].*?\\"');
			if (campaignRecord.sitescout_offertag != null) {
				var n = campaignRecord.sitescout_offertag.match(regextest);
				console.log('conversion_tag: ' + campaignRecord.sitescout_offertag);

				if (n.length >= 0) {
					var m = n[0].replace(/"/i,"");
					var o = m.split("/");
					var p = o.length -1;

					convertPixelId = o[p];
				} else {
					convertPixelId = "N/A";
				}			
			} else {
				convertPixelId = "N/A";
			}

			//Check Anti Tag
			var cmapAntiPixelId = '';
			var regextest = new RegExp('[a-z]+[:.].*?\\"');
			if (campaignRecord.sitescout_antitag != null) {
				var n = campaignRecord.sitescout_antitag.match(regextest);
				console.log('sitescout_antitag: ' + campaignRecord.sitescout_antitag);

				if (n.length >= 0) {
					var m = n[0].replace(/"/i,"");
					var o = m.split("/");
					var p = o.length -1;

					cmapAntiPixelId = o[p];
				} else {
					cmapAntiPixelId = "N/A";
				}			
			} else {
				cmapAntiPixelId = "N/A";
			}

			//Campaign Creative Sync
			/*
				if (allCreatives.length != 0 && campaignRecord.creatives != null) {
					if (campaignRecord.creatives.length != 0) {
						for (var c=0; c < allCreatives.length; c++) {
							//console.log(allCreatives[c]);				
							for (var d=0; d < campaignRecord.creatives.length; d++) {
								var primaryFlag = campaignRecord.creatives[d].primary;
								//console.log(campaignRecord.creatives[d]);
								var createId = campaignRecord.creatives[d].creativeId;

								if (campaignRecord.creatives[d].creativeId == allCreatives[c]._id) {
									campaignRecord.creatives[d] = allCreatives[c];
									campaignRecord.creatives[d].primary = primaryFlag;
									delete campaignRecord.creatives[d]._id;
									campaignRecord.creatives[d].creativeId = createId;
								}
							}
						}
					}
					
				}
				*/

				/*
				var creativeRec = allCreatives[0];
				//console.log(allCreatives[0]);
				creativeRec.campaignId = allCreatives[0]._id.toString();
				creativeRec.primary = true;
				delete creativeRec._id;
			*/

			// Campaign Url Creator 
			if (cmapPixelId != "N/A" && typeof campaignRecord.pxl_url == 'undefined' && typeof campaignRecord.pxl_url_id == 'undefined') {
				console.log('creating campaign Url');
				campaigns.createUrl(cmapPixelId, campaignRecord._id);
			}

			// Conversion Offer Url Creator 
			if (convertPixelId != "N/A" && typeof campaignRecord.pxl_url == 'undefined' && typeof campaignRecord.pxl_url_id == 'undefined') {
				console.log('creating campaign offer Url');
				campaigns.createOffer(convertPixelId, campaignRecord._id);
			}

			// Campaign Anti Url Creator 
			if (cmapAntiPixelId != "N/A" && typeof campaignRecord.pxl_url_anti == 'undefined' && typeof campaignRecord.pxl_url_anti_id == 'undefined') {
				console.log('creating campaign Url');
				campaigns.createAntiUrl(cmapAntiPixelId, campaignRecord._id);
			}

			//console.log(campaignRecord);
			var campaignRaw = campaignRecord._id;

			dbConn.campaigns.findAndModify({
			    query: { _id: campaignRaw },
			    update: { $set: 
			    	{ 
			    		status: campStatus,
			    		totalImpressions: impressionTotal,
			    		totalClicks: clickTotal,
			    		pixelId: cmapPixelId,
			    		offerId: convertPixelId,
			    		type: "full"
			    	} 
			    },
			    new: true
			}, function(err, doc, lastErrorObject) {
			    if (err) {
			    	console.log('Campaign update failure: ' + campaignRecord._id);
			    } else {
			    	console.log('Campaign update success');
			    }
			});
		}			
	}

	callback(true);
	
}

function queueCleaner(ssqueuer, merchqueuer, callback) {
	for (var i=0; i<ssqueuer.length; i++) {
		var ss = ssqueuer[i];
		if (ss._id) {
			dbConn.ssqueue.remove({_id : ss._id});
		} else {
			console.log(ss);
		}
	}
	for (var j=0; j<merchqueuer.length; j++) {
		var merch = merchqueuer[j];
		if (merch._id) {
			dbConn.merchqueue.remove({_id : merch._id}, function(err, results) {
			});
		} else {
			console.log(merch);
		}
	}

	/*
	accounts.getAllAccounts(function (accounts) {
		if (accounts) {
			for (var h=0; h<accounts.length; h++) {
				if (!accounts[h].keep) {
					dbConn.accounts.remove({_id: accounts[h]._id}, function(err2, results2) {

					});
				}
			}
			callback(true);
		} else {
			callback(true);
		}
		
	});*/
	callback(true);
}

function startQueueClean(req, res) {
	campaigns.getAllQueues(function (ssqueue, merchqueue) {
		if (ssqueue && merchqueue) {
			queueCleaner(ssqueue, merchqueue, function(results) {
				if (results) {
					res.send(200, 'worked');
				} else {
					res.send(200, 'failure');
				}
			})
		} else {
			res.send(200, 'failure');
		}
	});
}

function startCampaignProcess(req, cb) {
	campaigns.getAllCampaigns(function (allCampaigns) {
		//console.log(allCampaigns);
		if (allCampaigns) {
			campaigns.getAllCreatives(function (allCreatives) {
				//console.log(allCampaigns);
				if (allCreatives) {
					campaignProcessor(allCampaigns, allCreatives, function (result) {
						if (result) {
							cb(true);
						} else {
							cb(false);
						}
					});
					
				} else {
					console.log("I didn't work");
					cb(false);
				}
			});
			
		} else {
			console.log("I didn't work");
			cb(false);
		}
	});
				
}

module.exports.startCampaignProcess = startCampaignProcess;
module.exports.startQueueClean = startQueueClean;