var mongojs = require('mongojs');
var ObjectId = mongojs.ObjectId;
var dbConn = require('../api/db').connection;
var processor = require('./logic/processor.js');

function mainRun(req, callback) {
	
	processor.startCampaignProcess(req, function(result) {
		if (result) { 
			callback(true);
		} else {
			callback(false);
		}
	});
	
}

module.exports.mainRun = mainRun;