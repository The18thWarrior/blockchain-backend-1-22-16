var mongojs = require('mongojs');
var ObjectId = mongojs.ObjectId;
var dbConn = require('./db').connection;
var http = require('http');
var config = require('./config.js');

function getAllCampaigns(callback) {
	dbConn.campaigns.find(function(err, docs) {
    	// docs is an array of all the documents in mycollection
    	if (err) {
    		console.log("getAllCampaigns failed: metrics.js line 10");
    		callback(false);
    	} else {

    		callback(docs);	
    	}	
	});
}

function getAllCreatives(callback) {
    dbConn.creatives.find(function(err, docs) {
        // docs is an array of all the documents in mycollection
        if (err) {
            console.log("getAllCreatives failed: metrics.js line 10");
            callback(false);
        } else {
            //console.log(docs);
            callback(docs); 
        }   
    });
}

function getAllQueues(callback) {
	dbConn.ssqueue.find(function(err, ss) {
    	// docs is an array of all the documents in mycollection
    	if (err) {
    		console.log("getAllCampaigns failed: metrics.js line 10");
    		callback(false, false);
    	} else {
    		dbConn.merchqueue.find(function(err2, merch) {
    			if (err2) {
		    		console.log("getAllCampaigns failed: metrics.js line 10");
		    		callback(false, false);
		    	} else {
		    		console.log(ss.length);
		    		console.log(merch.length);
		    		callback(ss, merch);
		    	}
    			
    		})
    	}	
	});
}

function createUrl(pixelId, campaignId) {
    var urlPath = '';
                    
    var post_options = {
        host: config.serviceUrl,
        path: '/api/create/' + pixelId,
        method: 'GET',
    };

    var errorcheck = true;

    // Set up the request
    var post_req = http.request(post_options, function(res) {
      res.setEncoding('utf8');
      var str = '';
      
      res.on('data', function (chunk) {
            //console.log(chunk);
            str += chunk;   
        });

      res.on('error', function (error) {
            console.log("error: " + error.message);   
            errorcheck = false;     
      });

        res.on('end', function () {
            if (errorcheck) {
               dbConn.campaigns.findAndModify({
                    query: { _id: campaignId },
                    update: { $set: 
                        { 
                            pxl_url: config.serviceUrl + '/' + str,
                            pxl_url_id: str
                        } 
                    },
                    new: true
                }, function(err, doc, lastErrorObject) {
                    if (err) {
                        console.log('Create Url failure: ');
                    } else {
                        console.log('Create Url success');
                    }
                }); 

            } else {
                console.log('failure');
            }
            
        });
    });
    post_req.on('error', function(e) {
        console.log('internal run failure: ' + e.message);
        errorcheck = false; 
    });

    // post the data
    post_req.end();
}

function createAntiUrl(pixelId, campaignId) {
    var urlPath = '';
                    
    var post_options = {
        host: config.serviceUrl,
        path: '/api/create/' + pixelId,
        method: 'GET',
    };

    var errorcheck = true;

    // Set up the request
    var post_req = http.request(post_options, function(res) {
      res.setEncoding('utf8');
      var str = '';
      
      res.on('data', function (chunk) {
            //console.log(chunk);
            str += chunk;   
        });

      res.on('error', function (error) {
            console.log("error: " + error.message);   
            errorcheck = false;     
      });

        res.on('end', function () {
            if (errorcheck) {
               dbConn.campaigns.findAndModify({
                    query: { _id: campaignId },
                    update: { $set: 
                        { 
                            pxl_url_anti: config.serviceUrl + '/' + str,
                            pxl_url_anti_id: str
                        } 
                    },
                    new: true
                }, function(err, doc, lastErrorObject) {
                    if (err) {
                        console.log('Create antiUrl failure: ');
                    } else {
                        console.log('Create antiUrl success');
                    }
                }); 

            } else {
                console.log('failure');
            }
            
        });
    });
    post_req.on('error', function(e) {
        console.log('internal run failure: ' + e.message);
        errorcheck = false; 
    });

    // post the data
    post_req.end();
}

function createOffer(offerId, campaignId) {
    var urlPath = '';
                    
    var post_options = {
        host: config.serviceUrl,
        path: '/api/convertcreate/' + offerId,
        method: 'GET',
    };

    var errorcheck = true;

    // Set up the request
    var post_req = http.request(post_options, function(res) {
      res.setEncoding('utf8');
      var str = '';
      
      res.on('data', function (chunk) {
            //console.log(chunk);
            str += chunk;   
        });

      res.on('error', function (error) {
            console.log("error: " + error.message);   
            errorcheck = false;     
      });

        res.on('end', function () {
            if (errorcheck) {
               dbConn.campaigns.findAndModify({
                    query: { _id: campaignId },
                    update: { $set: 
                        { 
                            pxl_convert: config.serviceUrl + '/' + str,
                            pxl_convert_id: str
                        } 
                    },
                    new: true
                }, function(err, doc, lastErrorObject) {
                    if (err) {
                        console.log('Create Url failure: ');
                    } else {
                        console.log('Create Url success');
                    }
                }); 

            } else {
                console.log('failure');
            }
            
        });
    });
    post_req.on('error', function(e) {
        console.log('internal run failure: ' + e.message);
        errorcheck = false; 
    });

    // post the data
    post_req.end();
}



module.exports.getAllCampaigns = getAllCampaigns;
module.exports.getAllQueues = getAllQueues;
module.exports.getAllCreatives = getAllCreatives;
module.exports.createUrl = createUrl;
module.exports.createAntiUrl = createAntiUrl;
module.exports.createOffer = createOffer;