/* 
	PixelTag Internal Processor Admin Main file test
*/

var express = require("express");
var path = require('path');
var logfmt = require("logfmt");
var querystring = require('querystring');
var connect = require("connect");
var bodyParser = require("body-parser");
var cookieParser = require('cookie-parser');
var cookie = require('cookie');
var timeout = require('connect-timeout');
var geth = require("geth");
var gethInstall = require('../geth-bin/lib/install.js');

var app = express();

app.use(logfmt.requestLogger());
app.use(bodyParser());
app.use(cookieParser());
app.use(timeout(120000));




/*app.use('/', function(req, res) {
	res.sendfile('./merchentaAdmin.html');
});*/

//Git route for demoing access token geneneration
app.get('/start', startGeth);


app.get('/stop', function (req, resp) {

	geth.stop();

	resp.send(200, '<html><body style="background-color:black;"><img src="http://boards.420chan.org/1701/src/1402683986507.gif" /></body></html>');
	
});

var port = Number(process.env.PORT || 5000);
app.listen(port, function() {
  console.log("Listening on: " + port);
  //startGeth2();
});

function startGeth(req, resp) {
	var options = {
	    networkid: "58399",
	    port: 30303,
	    rpcport: 1987,
	    genesis: './genesis_block.json',
	    datadir: './.ethereum',
	    rpcapi: "eth,web3,personal,miner",
	    mine: null
	};

	try {
		geth.start(options, function (err, proc) {
		    if (err) {
		    	console.log(err);
		    	//resp.send(err);
		    	startGeth(req, resp);
		    } else {
		    	console.log('geth started');
		    	//console.log(proc);
		    	resp.status(200).send(String(proc));
			    // get your geth on!
		    }
		});
	} catch(e) {
		console.log('error starting geth');
		console.log(e);
		resp.send(200, options);
	}
}

function startGeth2() {
	var options = {
	    networkid: "58399",
	    port: 30303,
	    rpcport: 1987,
	    genesis: './genesis_block.json',
	    datadir: './.ethereum',
	    rpcapi: "eth,web3,personal,miner",
	    mine: null
	};

	try {
		geth.start(options, function (err, proc) {
		    if (err) {
		    	console.log(err);
		    	console.log('error geth');
		    	//resp.send(err);
		    	startGeth2();
		    } else {
		    	console.log('geth started');
		    	//console.log(proc);
		    	//resp.status(200).send(String(proc));
			    //get your geth on!
		    }
		});
	} catch(e) {
		console.log('error starting geth');
		console.log(e);
	}
}
