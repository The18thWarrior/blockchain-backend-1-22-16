var mongojs = require('mongojs');
var ObjectId = mongojs.ObjectId;
var dbConn = require('./db').connection;

function getAllAccounts(callback) {
	dbConn.accounts.find(function(err, docs) {
    	// docs is an array of all the documents in mycollection
    	if (err) {
    		console.log("getAllAccounts failed: metrics.js line 10");
    		callback(false);
    	} else {

    		callback(docs);	
    	}	
	});
}

module.exports.getAllAccounts = getAllAccounts;